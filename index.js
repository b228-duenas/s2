console.log("Hello World!");

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them
// Spaghetti Code - when codes is not organized enough that it becomes hard to work on it

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// Encapsulation - Organizes related information (properties) and behavior (methods) to belong to a single entity

// Activity

// =========
// Quiz
// =========

// What is the term given to unorganized code that's very hard to work with?

// - Spaghetti Code

// How are object literals written in JS?

// - Key value pair inside {}

// What do you call the concept of organizing information and functionality to belong to an object?

// - OOP

// If the studentOne object has a method named enroll(), how would you invoke it?

// - studentOne.enroll();

// True or False: Objects can have objects as properties.

// -True

// What is the syntax in creating key-value pairs?

// - properties: value

// True or False: A method can have no parameters and still work.

// - True

// True or False: Arrays can have objects as elements.

//  - True

// True or False: Arrays are objects.

//  - True

// True or False: Objects can have arrays as properties.

//  - True

// =====================
// Function Coding
// =====================
// Translate the other students from our boilerplate code into their own respective objects.

// Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

// Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

// Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  login() {
    console.log(`${this.name} has logged in`);
  },
  logout() {
    console.log(`${this.name} has logged out`);
  },
  listGrades() {
    console.log(`Student one's quarterly averages are ${this.grades}`);
  },
  avgGrade() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    return avg;
  },
  willPass() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 90) {
      return true;
    } else if (avg >= 85 && avg < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentTwo = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],
  login() {
    console.log(`${this.name} has logged in`);
  },
  logout() {
    console.log(`${this.name} has logged out`);
  },
  listGrades() {
    console.log(`Student one's quarterly averages are ${this.grades}`);
  },
  avgGrade() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    return avg;
  },
  willPass() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 90) {
      return true;
    } else if (avg >= 85 && avg < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentThree = {
  name: "Joe",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],
  login() {
    console.log(`${this.name} has logged in`);
  },
  logout() {
    console.log(`${this.name} has logged out`);
  },
  listGrades() {
    console.log(`Student one's quarterly averages are ${this.grades}`);
  },
  avgGrade() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    return avg;
  },
  willPass() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 90) {
      return true;
    } else if (avg >= 85 && avg < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentFour = {
  name: "Jessie",
  email: "jessie@mail.com",
  grades: [91, 89, 92, 93],
  login() {
    console.log(`${this.name} has logged in`);
  },
  logout() {
    console.log(`${this.name} has logged out`);
  },
  listGrades() {
    console.log(`Student one's quarterly averages are ${this.grades}`);
  },
  avgGrade() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    return avg;
  },
  willPass() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 85) {
      return true;
    } else {
      return false;
    }
  },
  willPassWithHonors() {
    let sum = this.grades.reduce((a, b) => a + b);
    let avg = sum / this.grades.length;
    if (avg >= 90) {
      return true;
    } else if (avg >= 85 && avg < 90) {
      return false;
    } else {
      return undefined;
    }
  },
};

// Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

// Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

// Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

// Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

// Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
  countHonorStudents() {
    let count = 0;
    this.students.forEach((e) => {
      if (e.willPassWithHonors()) {
        count++;
      }
    });
    return count;
  },
  honorsPercentage() {
    let count = 0;
    this.students.forEach((e) => {
      if (e.willPassWithHonors()) {
        count++;
      }
    });
    return (count / this.students.length) * 100;
  },
  retrieveHonorStudentInfo() {
    let arr = [];
    this.students.forEach((e) => {
      if (e.willPassWithHonors()) {
        arr.push(e);
      }
    });
    return arr;
  },
  sortHonorStudentsByGradeDesc() {
    let arr = [];
    this.students.forEach((e) => {
      if (e.willPassWithHonors()) {
        arr.push(e);
      }
    });
    return arr.sort(function (a, b) {
      return b.avgGrade() - a.avgGrade();
    });
  },
};
